{\rtf1\ansi\ansicpg1252\cocoartf1561\cocoasubrtf610
{\fonttbl\f0\fnil\fcharset0 HelveticaNeue;\f1\fmodern\fcharset0 Courier;}
{\colortbl;\red255\green255\blue255;\red0\green0\blue0;}
{\*\expandedcolortbl;;\cssrgb\c0\c0\c0;}
\paperw11900\paperh16840\margl1440\margr1440\vieww13920\viewh8400\viewkind0
\deftab720
\pard\pardeftab720\sl280\partightenfactor0

\f0\fs24 \cf2 \expnd0\expndtw0\kerning0
Internship\cf0 \kerning1\expnd0\expndtw0  M1, Boulinguiez Matthieu\
\pard\pardeftab720\sl280\partightenfactor0
\cf2 \expnd0\expndtw0\kerning0
\outl0\strokewidth0 \strokec2 \
\pard\pardeftab720\sl280\partightenfactor0
\cf2 \ul \ulc2 Internship subject :\ulnone  Pipeline to measure selection pressure on giant virus genes
\f1 \
\pard\tx566\tx1133\tx1700\tx2267\tx2834\tx3401\tx3968\tx4535\tx5102\tx5669\tx6236\tx6803\pardirnatural\partightenfactor0

\f0 \cf2 \outl0\strokewidth0 \
\pard\pardeftab720\sl280\partightenfactor0
\cf2 \outl0\strokewidth0 \strokec2 \
\pard\pardeftab720\sl280\partightenfactor0
\cf2 \ul \ulc2 Python libraries required :\cf2 \ulnone \outl0\strokewidth0 \
\pard\tx566\tx1133\tx1700\tx2267\tx2834\tx3401\tx3968\tx4535\tx5102\tx5669\tx6236\tx6803\pardirnatural\partightenfactor0
\cf2 - BioPython\
- Os \
- Re (regex)\
- Pandas
\fs78 \

\fs24 \
\pard\pardeftab720\sl280\partightenfactor0
\cf2 \ul \ulc2 \outl0\strokewidth0 \strokec2 Required programs :\cf2 \ulnone \outl0\strokewidth0 \
\pard\tx566\tx1133\tx1700\tx2267\tx2834\tx3401\tx3968\tx4535\tx5102\tx5669\tx6236\tx6803\pardirnatural\partightenfactor0
\cf2 - OrthoFinder\
- MAFFT\
- FastTree \
- Codeml (du package PAML)\
\
\pard\pardeftab720\sl280\partightenfactor0
\cf2 \outl0\strokewidth0 \strokec2 \
\pard\pardeftab720\sl280\partightenfactor0
\cf2 \ul \ulc2 Initialization of the pipeline :\cf2 \ulnone \outl0\strokewidth0 \
\cf2 \outl0\strokewidth0 \strokec2 Accession numbers used in the analysis must be entered in a file named: ids_file.txt present in the directory files_id_login. Accession numbers must be separated by line breaks, without spaces.\
\pard\pardeftab720\sl280\partightenfactor0
\cf2 \
\pard\pardeftab720\sl280\partightenfactor0
\cf2 The login_file.txt file present in this same directory must contain only your email address to allow the program to connect to the NCBI.\
}