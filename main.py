#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sat Apr 11 14:14:49 2020

@author: matt
"""

import script_Genbank as gb_file
import script_orthofinder as ortho
import script_mafft as mafft 
import script_align_nuc as align_nuc
import script_FastTree as tree
import script_codeml as codeml

def read_id_file(): 
    ''' Function to read ids_file and keep accession numbers in a list '''
    with open ("files_id_login/ids_file.txt", 'r') as ids_file: 
    # open ids_file
        list_lines_ids_file = ids_file.readlines()
        # keep each line in a list 
    list_accession_numbers = []
    # list of accession numbers
    for line in list_lines_ids_file:
    # for each line in ids_file
        if "\n" in line:
        # if line have \n 
            accession_number = line[:-1]
            # remove \n
            list_accession_numbers.append(accession_number)
            # add id in list
        else: 
            list_accession_numbers.append(line)
            # add accession number in list 
    return list_accession_numbers

list_accession_numbers = read_id_file()
# list of IDs

list_create_directory = ["files_id_login", "files_genbank", "files_CDS", 
                         "files_seq_prot", "files_orthologs", "files_seq_all_prot",
                         "files_align_mafft", "files_align_nuc", "files_tree", 
                         "files_codeml"]
# list of directory name

gb_file.create_directory(list_create_directory)
# command to create all directory 

gb_file.download_genbank_files(ids)
# command to download GenBank files

gb_file.keep_cds(list_accession_numbers)
# command to keep all CDS and proteins sequences in files

ortho.start_orthofinder()
# command to start OrthoFinder

directory = ortho.keep_directory_result()
# command to keep the name of result directory

dictionnary = ortho.filters_on_families(ortho.keep_familly_in_dict(directory), list_accession_numbers)
# command to keep each protein for each ortholog familly in dictionnary

ortho.keep_protein_sequences(dictionnary)
# command to keep each ortholog familly in files 

list_files_ortho  = ortho.list_files()
# command to list all ortholog familly

mafft.start_mafft(list_files_ortho)
# command to start protein alignement for each ortholog familly

dic_gaps = align_nuc.all_functions_in_one(list_files_ortho)
# command to introduce gaps in nucleotides sequences 

count_accession_numbers = len(list_accession_numbers)
# number of accession numbers 

align_nuc.write_files_align_nuc(list_files_ortho, dic_gaps, count_accession_numbers)
# command to write nucleotide alignement files

tree.all_functions_fasttree(list_files_ortho)
# command to load all fonciton to run FastTree

codeml.all_functions(list_files_ortho)
# command to start codeml 
