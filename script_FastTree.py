#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Apr 30 15:00:15 2020

@author: matt
"""

import os 
from Bio import SeqIO
import re 

def concatene_seq(list_orthologs):
    ''' Function to concatenate protein sequence and stock it in a dictionnary '''
    dictionnary_concatenate_sequence = {}
    # dictionnary contain in key the fasta id and in value all concatenates sequences 
    for ortholog in list_orthologs:
    # for each ortholog 
        
        with open ("files_align_mafft/align_mul_{}.txt".format(ortholog), "r") as file_align:
        # open protein alignement file for each ortholog 
            protein_file = SeqIO.parse(file_align, "fasta")
            # parse ortholog file to extract all protein 
            
            for protein in protein_file : 
            # for each protein in protein_file 
                fasta_id = protein.id 
                # keep protein identifiant
                split_fasta_id = fasta_id.split(".")
                # split fasta id by dot
                protein_id = split_fasta_id[0] + "." + split_fasta_id[1]
                # keep organism and genome id
                
                if protein_id not in dictionnary_concatenate_sequence :
                # if protein id not in dictionnary
                    dictionnary_concatenate_sequence[protein_id]=str(protein.seq)
                    # keep id and sequence in a dictionnary
                
                elif protein_id in dictionnary_concatenate_sequence:
                # if fasta identifiant already in dictionnary
                    dictionnary_concatenate_sequence[protein_id] += str(protein.seq)
                    # add sequence to the previous sequence
    return dictionnary_concatenate_sequence

def write_concatene_in_file(dictionnary_concatenate_sequence):
    ''' Function to create a file containing fasta id and concatenate sequence '''
    for key, value in dictionnary_concatenate_sequence.items():
    # for each fasta id and concatenate sequence
        save_path_concatenate = 'files_align_mafft'
        # path to directory 
        directory_concatenate = os.path.join(save_path_concatenate, "concatenate_sequence.txt")         
        # go to directory
        file_concatenate = open(directory_concatenate, "a")
        # create concatenate sequence file
        file_concatenate.write(">" + str(key) + "\n")
        # write in file the fasta id
        file_concatenate.write(str(value) + "\n\n")
        # write in file the concatene sequence
        file_concatenate.close()
        # file closen

def start_fasttree():
    ''' Function to start FastTree '''
    os.system("FastTree_cmd/FastTree -fastest files_align_mafft/concatenate_sequence.txt > files_tree/tree_file")
    # command to start FastTree on protein multiple alignement (with concatenate sequences)
    
def change_tree_file():
    ''' Function to remove bootstrap numbers '''
    with open ("files_tree/tree_file", "r") as tree_file :
    # open tree_file
        tree_line = tree_file.readline()
        # keep tree in a string
    
    pattern = re.compile(":(\d*\.\d*(\)\d*\.\d*)?)")
    # regex to remove bootstrap numbers
    
    def my_replace(match):
        ''' Function to replace match with ) by )'''
        return match[0].count(')')*')'

    sub_text = re.sub(pattern, my_replace, tree_line)
    # replace pattern in tree_file 
    return sub_text

def rewrite_tree_file(sub_text):
    ''' Function to rewrite tree_file '''
    tree = open("files_tree/tree_file", "w")
    # rewrite tree_file
    tree.write(sub_text)
    # change tree_file
    tree.close()
    # close tree_file

def all_functions_fasttree(list_orthologs):
    ''' Function to load all functions to run FastTree and to remove bootstrap numbers from tree_file'''
    write_concatene_in_file(concatene_seq(list_orthologs))
    # command to create a file containing fasta identifiant and concatenate sequences from a dictionnary
    start_fasttree()
    # command to start FastTree
    rewrite_tree_file(change_tree_file())
    # command to remove bootstrap in tree_file and to rewrite tree_file 
