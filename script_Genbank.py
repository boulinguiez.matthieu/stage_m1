#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Apr 14 10:54:02 2020

@author: matt
"""

from Bio import Entrez
from Bio import SeqIO
import os
import re 

def create_directory(list_directory):
    ''' Function to create all directories '''
    for directory in list_directory:
    # for each directory name in list
        os.system("mkdir {}".format(directory))
        # create directory

def read_login_file(): 
    ''' Function to read login_file and keep it in a variable '''
    with open ("files_id_login/login_file.txt", 'r') as login_file: 
    # open login_file 
        login = login_file.readline()
        # keep first line
    return login

Entrez.email = "".format(read_login_file())
# mail to be identify in NCBI

def download_genbank_files(list_accession_numbers):
    ''' Function to download GenBank files from accession numbers '''
    for accession_number in list_accession_numbers: 
    # for each accession number in list
        handle = Entrez.efetch(db = "nucleotide", id = accession_number, rettype = "gb", retmote = "text") 
        # variable contening genbank file 
        save_path = 'files_genbank'
        # path to directory 
        directory = os.path.join(save_path, accession_number + ".txt")         
        # go to directory
        genbank_file = open(directory, "w")
        # create genbank file
        genbank_file.write(handle.read() + "\n\n")
        # write genbank file
        genbank_file.close()
        # close genbank_file

def keep_cds(list_accession_number): 
    ''' Function to read a Genbank file, 
        create a fasta id, 
        keep protein and nucleic acid sequence '''
    list_stop = ["TGA", "TAG", "TAA"]
    # list of stop codons
    for accession_number in list_accession_number: 
    # for each accession number in list
        
        with open ("files_genbank/{}.txt".format(accession_number), "r") as genbank_file:
        # open genbank file
            file_gbk = SeqIO.read(genbank_file, "genbank")
            # read genbank file
            pattern = re.compile("[^a-zA-Z0-9_]")
            # accept just alpha-numeric caracters
            organism = file_gbk.annotations["source"].replace(" ", "_")
            # variable contain the organism without spaces
            organism = pattern.sub("", organism)
            # apply pattern on organism and remove caracters 
            
            # if len(organism)>32 :
            #     organism = organism[0:31]
            
            genome_id = file_gbk.id
            # variable contain genome id

            split_genome_id = genome_id.split(".")
            # split by dot
            genome_id = split_genome_id[0]
            # keep genome id without version
            
            for feature in file_gbk.features:
            # for each feature in features
                
                if feature.type == "CDS": 
                # if feature is a CDS
                    
                    try:
                        cds_id = feature.qualifiers["protein_id"]
                        # list contain the protein id 
                        split_protein_id = cds_id[0].split(".")
                        # split by dot
                        cds_id = split_protein_id[0]
                        # keep protein id without version
                        
                    except KeyError: 
                        try : 
                            cds_id = feature.qualifiers["gene"]
                            # list contain the gene id 
                            cds_id = cds_id[0].replace("-", "_")
                            # replace dash by underscore
                        
                        except KeyError: 
                            cds_id = feature.qualifiers["locus_tag"]
                            # list contain the protein id 
                            cds_id = cds_id[0].replace("-", "_")
                            # replace dash by underscore

                    fasta_id = ">" + str(organism) + "." + str(genome_id) + "." + str(cds_id)
                    # variable contain fasta id
                    nucleotide_sequence = feature.extract(file_gbk.seq)
                    # keep nucleotide sequence
                    
                    if nucleotide_sequence[-3:] in list_stop: 
                    # if CDS have stop codon
                        nucleotide_sequence = nucleotide_sequence[:-3]
                        # remove stop codon
                    
                    protein_sequence = nucleotide_sequence.translate(table = 1)
                    # translate in protein sequence
                
                    save_path_nuc = 'files_CDS'
                    # path to directory 
                    directory_nuc = os.path.join(save_path_nuc, accession_number + "_nuc.txt")         
                    # go to directory
                    file_cds = open(directory_nuc, "a")
                    # create CDS file
                    file_cds.write(fasta_id + "\n")
                    # write in file the fasta id
                    file_cds.write(str(nucleotide_sequence) + "\n\n")
                    # write in file the nucleotide sequence
                    file_cds.close()
                    # file closen

                    save_path_prot = 'files_seq_prot'
                    # path to directory 
                    directory_prot = os.path.join(save_path_prot, accession_number + "_prot.fasta")         
                    # go to directory
                    file_prot = open(directory_prot, "a")
                    # create CDS file
                    file_prot.write(fasta_id + "\n")
                    # write in file the fasta id
                    file_prot.write(str(protein_sequence) + "\n\n")
                    # write in file the protein sequence
                    file_prot.close()
                    # file closen
                    
                    save_path_all_prot = 'files_seq_all_prot'
                    # path to directory
                    directory_all_prot = os.path.join(save_path_all_prot, "all_seq_prot.fasta")         
                    # go to directory
                    file_all_prot = open(directory_all_prot, "a")
                    # create file with all proteins
                    file_all_prot.write(fasta_id + "\n")
                    # write in file the fasta identifiant
                    file_all_prot.write(str(protein_sequence) + "\n\n")
                    # write in file the protein sequence
                    file_all_prot.close()
                    # file closen
