#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Apr 28 13:03:51 2020

@author: matt
"""

from Bio import SeqIO
import os

def keep_align_in_dic(list_orthologs):
    ''' Function to keep protein in dictionnary '''
    dictionnary_align = {}
    # dictionnary with id in key and the sequence align in value 
    for ortholog in list_orthologs:
    # for each ortholog number 
        
        with open ("files_align_mafft/align_mul_{}.txt".format(ortholog), "r") as file_mafft:
        # read protein alignement file 
            proteins_file = SeqIO.parse(file_mafft, "fasta")
            # parse ortholog file to extract all proteins
            
            for protein in proteins_file: 
            # for each protein in proteins_file
                dictionnary_align[protein.id] = protein.seq
                # keep id and sequence in a dictionnary
    return dictionnary_align

def change_protein_in_code(dictionnary_align_proteins):
    ''' Fonction to change sequence in code sequence '''
    dictionnary_code = {}
    # dictionnary with id in key and code sequence in value
    for key, value in dictionnary_align_proteins.items():
    # for each protein in dictionnary
        code_sequence = ""
        # new sequence of protein

        for caracter in value:
        # for each amino acid or gap in protein sequence 
            
            if caracter == "-":
            # if caracter is a gap 
                code_sequence += "-"
                # add gap in sequence 
            
            else :
            # if caracter is a amino acid 
                code_sequence += "N"
                # replace it by a N 
        
        dictionnary_code[key] = code_sequence
        # keep id and code sequence in dictionnary
    return dictionnary_code

def keep_nuc_seq_in_dic(dictionnary_align_proteins):
    ''' Function to keep nucleotide sequence in dictionnary '''
    dictionnary_nucleotide_sequence = {}
    # dictionnary with id in key and nucleotide sequence in value
    for key in dictionnary_align_proteins.keys():
    # for each key in protein dictionnary
        key_split = key.split(".")
        # split by dot 
        genome_id = key_split[1]
        # keep genome id 
        fasta_id = ">" + key + "\n"
        # construct fasta id
        
        with open("files_CDS/{}_nuc.txt".format(genome_id), "r") as cds_file: 
        # read nucleotide file with good genome id 
            list_lines_cds = cds_file.readlines() 
            # keep all lines in a list
        
        for position in range(len(list_lines_cds)):
        # each position in list
            
            if list_lines_cds[position] == fasta_id:
            # if element of list is fasta id
                dictionnary_nucleotide_sequence[key] = list_lines_cds[position+1].replace("\n", "")
                # keep id and nucleotide sequence 
    return dictionnary_nucleotide_sequence

def introduction_gaps(dictionnary_code, dictionnary_nucleotide_sequence):
    ''' Fonction to introduce gaps in nucleotide sequence with protein alignements '''
    dictionnary_align_nucleotide = {}
    # dictionnary with id in key and nucleotide sequence with gaps in value
    for key_prot, val_prot in dictionnary_code.items():
    # for each key and value in protein dictionnary
        
        for key_nuc, val_nuc in dictionnary_nucleotide_sequence.items():
        # for each key and value in nucleotide dictionnnary 
            
            if key_prot == key_nuc: 
            # if keys are egals 
                nucleotide_sequence_gaps = ""
                # nucleotide sequence with gaps
                counter = 0
                # counter to read nucleotide sequence 
                
                for caracter in val_prot:
                # for each caracter in code sequence 
                    
                    if caracter == "-": 
                    # if caracter is a gap 
                        nucleotide_sequence_gaps += "---"
                        # add 3 gaps 

                    elif caracter == "N": 
                    # if caracter is a amino acid
                        nucleotide_sequence_gaps += val_nuc[counter:counter+3]
                        # add 3 nucleotides 
                        counter += 3
                        # add 3 in counter 

                # seq_nuc_gap_sans_gap = nucleotide_sequence_gaps.replace("-", "")
                # # nucleotide sequence obtain without gaps
                
                # if len(val_nuc) != len(seq_nuc_gap_sans_gap):
                # # if lenght of nucleotide sequence and the sequence obtain are differents 
                #     nucleotide_sequence_gaps += val_nuc[-3:]
                #     # add last codon (STOP)
                
                dictionnary_align_nucleotide[key_nuc] = nucleotide_sequence_gaps
                # keep id and nucleotide sequence with gaps in dictionnary
    return dictionnary_align_nucleotide

def write_files_align_nuc(list_orthologs, dictionnary_align_nucleotide, sequence_number):
    ''' Function to write nucleotide alignements files '''
    for ortholog in list_orthologs:
    # for each ortholog name 
        
        with open ("files_align_mafft/align_mul_{}.txt".format(ortholog), "r") as align_file:
        # read protein alignement
            list_line_align = align_file.readlines()
            # keep all lines in a list
        save_path_align_nuc = 'files_align_nuc'
        # path to directory 
        directory_align_nuc = os.path.join(save_path_align_nuc, ortholog + "_align_nuc.txt")         
        # go to directory
        file_align_nuc = open(directory_align_nuc, "a")
        # create nucleotide alignement file
        
        for position in range(len(list_line_align)):
        # for each position in list
            
            if list_line_align[position].startswith(">"): 
            # if list element start with > 
                fasta_id = list_line_align[position].replace("\n", "").replace(">", "")
                # keep fasta id
                sequence_gap = dictionnary_align_nucleotide[fasta_id]
                # keep nucleotide sequence with gaps 
                split_fasta_id = fasta_id.split(".")
                # split by dot the fasta id
                fasta_id = split_fasta_id[0] + "." + split_fasta_id[1]
                # keep organism and genome id 
                len_sequence = len(sequence_gap)
                # keep lenght of sequence
                file_align_nuc.write(fasta_id + "\n")
                # write in file the fasta identifiant
                file_align_nuc.write(str(sequence_gap) + "\n\n")
                # write in file the nucleotide sequence with gaps
        
        file_align_nuc.close()
        # file closen
        
        with open("files_align_nuc/{}_align_nuc_test.txt".format(ortholog), 'a') as write_file:
        # open new file
            
            with open("files_align_nuc/{}_align_nuc.txt".format(ortholog), 'r') as read_file:
            # open alignement file
                write_file.write(str(sequence_number) + " " + str(len_sequence) + "\n\n")
                # write in first line the number of sequence and the lenght of sequences
                
                for line in read_file:
                # for each line in alignement file
                    write_file.write(line)
                    # write line
        
        os.remove("files_align_nuc/{}_align_nuc.txt".format(ortholog))
        # remove old alignement file
        os.rename("files_align_nuc/{}_align_nuc_test.txt".format(ortholog), "files_align_nuc/{}_align_nuc.txt".format(ortholog))
        # rename new alignement file


def all_functions_in_one(list_orthologs):
    ''' Function to call all functions '''
    dictionnary_align_proteins = keep_align_in_dic(list_orthologs)
    dictionnary_code = change_protein_in_code(dictionnary_align_proteins)
    dictionnary_nucleotide_sequence = keep_nuc_seq_in_dic(dictionnary_code)
    dictionnary_align_nucleotide = introduction_gaps(dictionnary_code, dictionnary_nucleotide_sequence)
    return dictionnary_align_nucleotide
