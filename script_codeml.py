#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Apr 30 15:13:52 2020

@author: matt
"""
import os 
import pandas as pd 
import script_orthofinder as ortho


def keep_control_file():
    ''' Function to read and to keep control file in a other file '''
    with open ("paml4.9j/codeml.ctl", "r") as control_file : 
    # open control file
        list_lines = control_file.readlines()
        # keep all lines in a list 
    return list_lines

def start_codeml(list_orthologs, list_lines):
    ''' Function to start codeml '''
    for ortholog in list_orthologs:
    # for each ortolog
        other_file = open("paml4.9j/codeml.ctl", "w")
        # open control file

        for position in range(len(list_lines)):
        # for each position in list
            file_input = "files_align_nuc/{}_align_nuc.txt".format(ortholog)
            # path to alignement file
            first_line = "      seqfile = " + file_input + " * sequence data filename\n"
            # create the input line
            list_lines[0] = first_line
            # change input line 
            file_output = "files_codeml/{}_ml".format(ortholog)
            # path to output file
            third_line = "      outfile = " + file_output + "           * main result file name\n"
            # create ouput line
            list_lines[2] = third_line
            # change ouput line
            other_file.write(list_lines[position])
            # rewrite control file
        
        other_file.close()
        # close new control file
        os.system("paml4.9j/bin/codeml paml4.9j/codeml.ctl")
        # command to start codeml 

def list_files():
    ''' Function to count how many files are in a directory '''
    list_files = os.listdir("files_codeml")
    # command to list and count files in a directory 
    list_file_name = []
    # list with all files names
    list_file_ortholog = []
    # list of ortholog name 
    for position in range(len(list_files)) :
    # for each ortholog names 
        list_file_name.append(list_files[position].replace("_ml", ""))
        # append in list the ortholog name without extension 

    for element in range(len(list_file_name)):
    # for each file name
        
        if list_file_name[element].startswith("OG"):
        # if file start by OG
            list_file_ortholog.append(list_file_name[element])
            # add ortholog name in list
    return list_file_ortholog

def read_ml_file(list_orthologs):
   ''' Function to keep informations from ml files '''
   list_info_ortho = []
   list_info_positions = []
   list_info_tree_len_dn = []
   list_info_tree_len_ds = []
   list_info_tree_len = []
   list_info_ratio = []
   # lists to keep each informations
   
   for ortholog in list_orthologs:
   # for each ortholog

       with open ("files_codeml/{}_ml".format(ortholog), "r") as ml_file : 
       # open each ml file
           list_lines_ml = ml_file.readlines()
           # keep lines of ml file in list 
       
       list_info_ortho.append(ortholog)
       # keep ortholog name
       
       for number_line in range(len(list_lines_ml)):
       # for each position in list of lines
           
           if list_lines_ml[number_line].startswith(" branch"):
           # if line start by branch
               split_branch = list_lines_ml[number_line + 2].split()
               # split line 
               nb_positions = str(float(split_branch[2]) + float(split_branch[3]))
               # calculate number of position
               nb_positions = nb_positions.replace(".", ",")
               # change dot by coma
               list_info_positions.append(nb_positions)
               # keep number of position

           if list_lines_ml[number_line].startswith("tree length for dN:"):
           # if line start by tree length for dN:
                split_tree_len_dn = list_lines_ml[number_line].split()
                # split line
                tree_len_dn = split_tree_len_dn[4]
                # keep number for dN
                tree_len_dn = tree_len_dn.replace(".", ",")
                # change dot by ,
                list_info_tree_len_dn.append(tree_len_dn)
                # add in list 
                
                split_tree_len_ds = list_lines_ml[number_line + 1].split()
                # split line
                tree_len_ds = split_tree_len_ds[4]
                # keep number for dS
                tree_len_ds = tree_len_ds.replace(".", ",")
                # change dot by coma
                list_info_tree_len_ds.append(tree_len_ds)
                # add in list

           if list_lines_ml[number_line].startswith("tree length ="):
           # if line start by tree lenght = 
                split_tree_len = list_lines_ml[number_line].split()
                # split line
                tree_len = split_tree_len[3]
                # keep tree lenght
                tree_len = tree_len.replace(".", ",")
                # change dot by coma
                list_info_tree_len.append(tree_len)
                # add in list 

           if list_lines_ml[number_line].startswith("omega"):
           # if line start by omega 
                split_line = list_lines_ml[number_line].split()
                # split line
                ratio = split_line[3]
                #keep omega ratio
                ratio = ratio.replace(".", ",")
                # change dot by coma
                list_info_ratio.append(ratio)
                # add in list

   dic_informations = {'Ortholog_group' : list_info_ortho, 
                        'number_of_positions' : list_info_positions, 
                        'tree_lenght_for_dN' : list_info_tree_len_dn, 
                        'tree_lenght_for_dS': list_info_tree_len_ds, 
                        'tree_lenght' : list_info_tree_len, 
                        'omega_ratio' : list_info_ratio}
   # create dictionnary with all informations
   
   df = pd.DataFrame(dic_informations)
   # create dataframe with pandas
   
   df.to_csv('files_codeml/result_file.csv')     
   # write tabular file

def all_functions(list_orthologs):
    ''' All function to start codeml and write tabular file '''
    start_codeml(list_orthologs, keep_control_file())
    # command to change control file and start codeml
    read_ml_file(list_files())
    # command to read ml files and write a tabular file
