#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Apr 23 16:43:26 2020

@author: matt
"""

import os 

def start_mafft(list_files): 
    ''' Function to start MAFFT algorithm '''
    for ortholog in list_files:
    # for each ortholog number 
        os.system("mafft-mac/mafft.bat --auto files_orthologs/{}.fasta > files_align_mafft/align_mul_{}.txt".format(ortholog, 
                                                                                                                    ortholog))
        # start mafft algorithm for each ortholog number 
