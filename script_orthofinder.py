#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Apr 21 15:50:12 2020

@author: matt
"""

from Bio import SeqIO
import os 

def start_orthofinder():
    ''' Function to start OrthoFinder algorithm  '''
    os.system("orthofinder-2.3.11-0/bin/orthofinder -f files_seq_prot")
    # command to start OrthoFinder algorithm 

def keep_directory_result():
    ''' Function to keep the directory name with results of OrthoFinder '''
    directory = os.listdir("files_seq_prot/OrthoFinder")
    # go to directory
    for element in directory: 
    # for each file or directory in files_seq_prot/OrthoFinder

        if element.startswith("Results_"):
        # if directory start by Results_
            directory = str(directory[0])
            # keep name of this directory
    return directory

def keep_familly_in_dict(result_directory):
    ''' Function to create a dictionnary, to keep all orthologs groups and their accession numbers '''
    dictionnary_orthologs = {}
    # dictionnary with in key the ortholog group and in value a list of accession numbers
    with open ('files_seq_prot/OrthoFinder/{}/Orthogroups/Orthogroups.txt'.format(result_directory), 'r') as orthologs_file: 
    # open file with orthologs families
        list_line_orthologs = orthologs_file.readlines()
        # keep each line in a list
    
    for line in list_line_orthologs:
    # for each line in list
        line_split = line.replace("\n", "").replace(":", "").split()
        # split line and replace some caracters
        list_accession_numbers = list(line_split[1:])
        # keep all accession numbers in a list 
        dictionnary_orthologs[line_split[0]] = list_accession_numbers
        # creat dictionnary
    return dictionnary_orthologs

def filters_on_families(dictionnary_orthologs, list_accession_numbers):
    ''' Function to apply filters to takes one sequence by genome '''
    dictionnary_orthologs_sequence = {}
    # dictinnary of orthologs sequences
    for key, value in dictionnary_orthologs.items(): 
    # for each key and value in dictionnary
        
        if len(value) == len(list_accession_numbers):
        # if value have same lenght as list of accession numbers
            list_ortho = []
            # list to comapre elements in value of dictionnary

            for element in value:
            # for each accession number in value
                element_split = element.split(".")
                # split by dot
                organism_genenome_id = element_split[0] + "." + element_split[1]
                # create id without protein id
                list_ortho.append(organism_genenome_id)
                # add id in list
            
            list_ortho = set(list_ortho)
            # list without duplicates 
            
            if len(value) == len(list_ortho):
            # if value haven't duplicates
                dictionnary_orthologs_sequence[key] = value
                # add ortholog in dictionnary_orthologs_sequence
    return dictionnary_orthologs_sequence

def keep_protein_sequences(dictionnary_familly):
    ''' Function to keep nucleic sequence for each familly ''' 
    fasta_file = SeqIO.parse(open("files_seq_all_prot/all_seq_prot.fasta"),'fasta')
    # read fasta file with all protein sequence
    for sequence in fasta_file:
    # for each fasta sequence 
        name = str(sequence.id)
        # keep id
        sequence = str(sequence.seq)
        # keep nucleic sequence

        for key, value in dictionnary_familly.items():
        # for each key and value in dictionnary_familly
            
            if name in value: 
            # if id is in value of dictionnary
                save_path_ortho = 'files_orthologs'
                # path to directory 
                directory_ortho = os.path.join(save_path_ortho, str(key) + ".fasta")         
                # go to directory
                file_ortho = open(directory_ortho, "a")
                # create file with all proteins of one familly
                file_ortho.write(">" + name + "\n")
                # write in file the fasta id
                file_ortho.write(sequence + "\n\n")
                # write in file protein sequence
                file_ortho.close()
                # file closen


def list_files():
    ''' Function to count how many files are in files_orthologs '''
    list_files = os.listdir("files_orthologs")
    # command to list and count files in a directory 
    list_without_extension = []
    # list of ortholog names without extension 
    for file in list_files:
    # for each ortholog names 
        list_without_extension.append(file.replace(".fasta", ""))
        # append in list the ortholog name without extension 
    return list_without_extension
