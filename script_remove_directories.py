#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Apr 20 16:19:36 2020

@author: matt
"""

import os

list_delete_directories = ["files_CDS", "files_seq_prot", "files_all_prot", 
                         "files_orthologs", "files_align_mafft", "files_seq_all_prot",
                         "files_align_nuc", "files_tree", "files_codeml"]
# list of directory name 

def remove_directory(list_directories):
    ''' Function to remove all directories '''
    for directory in list_directories:
    # for each directory in list
        os.system("rm -rf {}".format(directory))
        # remove directory

remove_directory(list_delete_directories)
